###  Final-project-aws-code-pipeline

---

## Table Of Contents[![](./docs/img/pin.svg)](#table-of-contents)

1. [Aciklama](#aciklama)


---
### Used Technologies
![Linux](https://img.shields.io/badge/-Linux-05122A?style=flat&logo=linux)&nbsp;
![VisualCode](https://img.shields.io/badge/-VisualCode-05188B?style=flat&logo=visualcode)&nbsp;
![Bash](https://img.shields.io/badge/-Bash-008000?style=flat&logo=bash)&nbsp;
![AWS](https://img.shields.io/badge/-AWS-05188B?style=flat&logo=aws)&nbsp;
![Docker](https://img.shields.io/badge/-Docker-4682B4?style=flat&logo=docker)&nbsp;

#### Explanation [![](./docs/img/pin.svg)](#aciklama)
 
Tried to apply the most appropriate DevOps standards.
Written a script ( Ansible & Shell ) that sends an Email notification if disk usage exceeds 90% on an operating system using any Linux distribution.








